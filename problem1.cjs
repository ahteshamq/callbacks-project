const fs = require("fs");

function problem1() {
  fs.mkdir("randomJson", function (err) {
    if (err) {
      console.error("File could not be created. ", err);
    } else {
      console.log("Folder created successfully.");
      console.log("File creation starting.");
      let randomInt = Math.floor(Math.random() * (10 - 1 + 1) + 1); // 10 is max and 1 is min limit for random number.
      let filenames = [];
      for (let index = 1; index <= randomInt; index++) {
        let filename = `jsonFile${index}.json`;
        let obj = { [filename]: "Inside " + filename + "." };
        fs.writeFile(
          `randomJson/${filename}`,
          JSON.stringify(obj),
          function (err) {
            if (err) {
              console.error(`${filename} could not be created: `, err);
            } else {
              console.log(`File ${filename} created`);
              filenames.push(filename);
              if (filenames.length === randomInt) {
                console.log("All files created successfully.");
                console.log("Starting file deletion");
                for (let index = 0; index < filenames.length; index++) {
                  fs.unlink(`randomJson/${filenames[index]}`, function (err) {
                    if (err) {
                      console.error(
                        `${filenames[index]} could not be deleted: `,
                        err
                      );
                    } else {
                      console.log(`${filenames[index]} deleted successfully.`);
                    }
                  });
                }
              }
            }
          }
        );
      }
    }
  });
}

module.exports = problem1;
