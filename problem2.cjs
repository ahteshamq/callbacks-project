const fs = require("fs");

function problem2(lipsumPath) {
  fs.readFile(lipsumPath, "utf-8", function (err, data) {
    if (err) {
      console.error("Error: could not read content: ", err);
    } else {
      let upper = data.toUpperCase();
      fs.writeFile("upper.txt", upper, "utf-8", function (err) {
        if (err) {
          console.error("Error: could not write content in uppercase: ", err);
        } else {
          console.log("File upper.txt written successfully.");
          fs.writeFile("filenames.txt", "upper.txt\n", "utf-8", function (err) {
            if (err) {
              console.error(
                "Could not write filename upper.txt on filenames.txt",
                err
              );
            } else {
              console.log("File name upper.txt written on filenames.txt");
              fs.readFile("upper.txt", "utf-8", function (err, data) {
                if (err) {
                  console.error("Could not read upper.txt: ", err);
                } else {
                  let lower = data.toLowerCase().split(".");
                  let split = lower.reduce((pre, next) => {
                    return (pre += next.trim() + ".\n");
                  });
                  fs.writeFile("split.txt", split, "utf-8", function (err) {
                    if (err) {
                      console.error(
                        "Could not write splited content on split.txt",
                        err
                      );
                    } else {
                      console.log("Splitted content written on split.txt");
                      fs.appendFile(
                        "filenames.txt",
                        "split.txt\n",
                        function (err) {
                          if (err) {
                            console.error(
                              "Could not write filename split.txt on filenames.txt",
                              err
                            );
                          } else {
                            console.log(
                              "Filename split.txt written on filenames.txt"
                            );
                            fs.readFile(
                              "split.txt",
                              "utf-8",
                              function (err, data) {
                                if (err) {
                                  console.error(
                                    "Could not read content from split.txt: ",
                                    err
                                  );
                                } else {
                                  let sortContent = data.split("\n").sort();
                                  let sortString = sortContent.reduce(
                                    (pre, next) => {
                                      return (pre += next.trim() + "\n");
                                    }
                                  );
                                  fs.writeFile(
                                    "sort.txt",
                                    sortString,
                                    "utf-8",
                                    function (err) {
                                      if (err) {
                                        console.error(
                                          "Could not write splitted content on split.txt",
                                          err
                                        );
                                      } else {
                                        console.log(
                                          "Sorted content written on sort.txt"
                                        );
                                        fs.appendFile(
                                          "filenames.txt",
                                          "sort.txt\n",
                                          function (err) {
                                            if (err) {
                                              console.error(
                                                "Could not write filename split.txt on filenames.txt",
                                                err
                                              );
                                            } else {
                                              console.log(
                                                "Filename sort.txt written on filenames.txt"
                                              );
                                              fs.readFile(
                                                "filenames.txt",
                                                "utf-8",
                                                function (err, data) {
                                                  if (err) {
                                                    console.error(
                                                      "Could not read file names from filenames.txt",
                                                      err
                                                    );
                                                  } else {
                                                    data
                                                      .split("\n")
                                                      .map((file) => {
                                                        if (file !== "") {
                                                          fs.unlink(
                                                            file,
                                                            function (err) {
                                                              if (err) {
                                                                console.error(
                                                                  `Could not delete ${data}: `,
                                                                  err
                                                                );
                                                              } else {
                                                                console.log(
                                                                  file +
                                                                    " deleted successfully."
                                                                );
                                                              }
                                                            }
                                                          );
                                                        }
                                                      });
                                                  }
                                                }
                                              );
                                            }
                                          }
                                        );
                                      }
                                    }
                                  );
                                }
                              }
                            );
                          }
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = problem2;
