const fs = require("fs");
const path = require("path");
const problem2 = require("../problem2.cjs");

const lipsumPath = path.join(__dirname, "lipsum.txt");

problem2(lipsumPath);
